
let miMapa= L.map("mapid");

miMapa.setView([4.6325305,-74.18437],13);
let miProveedor= L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
);
miProveedor.addTo(miMapa);

//crear un objeto marcador
let miMarcador=L.marker([4.6325305,-74.18437]);

miMarcador.bindPopup("<b>Arveja</b><br>2021/01/12<br>Pisym sativum").openPopup();
miMarcador.addTo(miMapa);


//json
let circle=L.circle([4.6325305,-74.18437],{
    color:"blue",
    fillColor:"blue",
    fillOpacity:0.5,
    radius:B3,

});

circle.addTo(miMapa);
let miGeoJSON= L.geoJSON(sitio);

miGeoJSON.addTo(miMapa);

let miTitulo=document.getElementById("titulo");
miTitulo.textContent=sitio.features[0].properties.popupContent;

let miDescripcion=document.getElementById("descripcion");
miDescripcion.textContent=sitio.features[0].properties.description;
